import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class Application1 {

    public static void main(String[] args) throws Exception{
        FileInputStream fstream = new FileInputStream("c:\\dev\\1.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String reader = br.readLine();
        String[] splitted = reader.split(",");
                
        List<Integer> intList = new ArrayList<Integer>();
        
        for (int i = 0; i < splitted.length; i++) {
            intList.add(Integer.parseInt(splitted[i]));
        }
        
        for (int i = intList.size()-1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (intList.get(j) > intList.get(j + 1)) {
                    int tmp = intList.get(j);
                    intList.set(j, intList.get(j+1));
                    intList.set(j+1, tmp);
                    
                }
            }
        }
        System.out.println(intList);
        
        for (int i = intList.size()-1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (intList.get(j) <= intList.get(j + 1)) {
                    int tmp = intList.get(j);
                    intList.set(j, intList.get(j+1));
                    intList.set(j+1, tmp);
                    
                }
            }
        }
        System.out.println(intList);
        
        //или можно так 
        Collections.sort(intList);    
        System.out.println(intList);
        
        Collections.reverse(intList);
        System.out.println(intList); 
    }
}