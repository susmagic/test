public class Application2 {

    public static void main(String[] args) throws Exception{
        System.out.println(factorial(20));
    }
    
    public static long factorial (long num) {
        return (num == 0) ? 1 : num * factorial (num - 1);
    }
}